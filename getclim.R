# get clim function

getclim<-function(x){
  lat<-x[2]
  lon<-x[1]
  p.soil<-as.data.frame(extract(soils, cbind(lon, lat)))
  p.PET<-as.data.frame(extract(PET, cbind(lon, lat)))
  p.Ann_PET<-as.data.frame(extract(AnnPET, cbind(lon, lat)))
  names(p.Ann_PET)<-c('PET_Ann')
  p.Ann_AI<-as.data.frame(extract(AnnAI, cbind(lon, lat)))
  names(p.Ann_AI)<-c('AI_Ann')
  BIO<-getData('worldclim', var='bio', lat=lat, lon=lon , res=0.5)
  Prec<-getData('worldclim', var='prec', lat=lat, lon=lon , res=0.5)
  tmean<-getData('worldclim', var='tmean', lat=lat, lon=lon , res=0.5)
  tmin<-getData('worldclim', var='tmin', lat=lat, lon=lon , res=0.5)
  alt<-getData('worldclim', var="alt", lat=lat, lon=lon , res=0.5)
  p.alt<-as.data.frame(extract(alt, cbind(lon, lat)))
  colnames(p.alt)<-c('Alt')
  p.BIO<-as.data.frame(extract(BIO, cbind(lon, lat)))
  colnames(p.BIO)<-substr(names(BIO), start = 1, stop = 5)
  p.prec<-as.data.frame(extract(Prec, cbind(lon, lat)))
  colnames(p.prec)<-substr(names(Prec), start = 1, stop = 6)
  p.tmean<-as.data.frame(extract(tmean, cbind(lon, lat)))
  colnames(p.tmean)<-substr(names(tmean), start = 1, stop = 7)
  p.frostfree<-data.frame(frostfreeperiod=sum((extract(tmin, cbind(lon, lat)))>0))
  p.SMN<-as.data.frame(extract(means, cbind(lon, lat)))
  colnames(p.SMN)<-paste('SMN_', 1:52, sep='')
  p.VHfreq<-as.data.frame(extract(VHI_freq, cbind(lon, lat)))
  colnames(p.VHfreq)<-paste('VHfreq_', 1:52, sep='')
  p.AI<-p.prec/p.PET
  colnames(p.AI)<-paste('AI_', 1:12, sep='')
  outrow<-cbind(lat, lon, p.SMN, p.VHfreq, p.BIO, p.tmean, p.frostfree, p.prec, p.Ann_PET, p.PET, p.AI, p.Ann_AI, p.soil)
  return(outrow)
}

